<?php

namespace App\Models;

use Illuminate\Http\Request;

use App\Models\DbTables\ParticipantMessenger;
use App\Models\DbTables\Participant;

use Log;

class ParticipantMessageModel extends MessengerModel
{
    /**
     * Message text received from user
     *
     * @var Array
     */
    protected $message_text = [];

    /**
     * Message buttons
     *
     * @var Array
     */
    protected $buttons = [];

    /**
     * Message text
     *
     * @var String
     */
    protected $message;

    /**
     * Instantiate conversation started
     *
     * @param  Illuminate\Http\Request  $request         Request object
     * @param  String                   $messenger_name  Messenger name
     * @return string
     */
    public function __construct()
    {
        parent::__construct();

        $this->setParticipantData();
    }

    /**
     * Set participant data
     *
     * @return void
     */
    protected function setParticipantData()
    {
        // Set participant id
        self::$participant_id = ParticipantMessenger::getParticipantIdByUid(
            $this->messenger_id,
            $this->request->sender['id']
        );

        // Set participant type
        if (!empty(self::$participant_id))
        {
            self::$participant_type = Participant::getParticipantTypeById($this->participant_id);
        }
    }

    public function getParticipantType()
    {
        return self::$participant_type;
    }
}
