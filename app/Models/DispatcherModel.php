<?php

namespace App\Models;

use App\Models\DbTables\Queue;
use App\Models\DbTables\Poll;
use App\Models\DbTables\Participant;
use App\Models\DbTables\ParticipantMessenger;
use App\Models\DbTables\ParticipantPoll;

use Paragraf\ViberBot\Bot;
use Paragraf\ViberBot\Model\ViberUser;
use Paragraf\ViberBot\TextMessage;
use Paragraf\ViberBot\Event\MessageEvent;

use Paragraf\ViberBot\Model\Button;
use Paragraf\ViberBot\Model\Keyboard;
use Paragraf\ViberBot\Messages\Message;

class DispatcherModel
{
    protected $request;
    protected $participant_id;
    protected $type;
    protected $object_id;

    /**
     * Check if string is JSON
     *
     * @param  String  $string  String to check
     * @return Bool
     */
    public function DispatchNext($request, int $participant_id)
    {
        $this->request = $request;
        $this->participant_id = $participant_id;

        // Check queue
        $queue = Queue::getPrticipantPending($this->participant_id);

        if (empty($queue['type']))
        {
            return;
        }

        $this->type = $queue['type'];
        $this->object_id = $queue['object_id'];

        switch ($queue['type']) {
            case 'poll':
                $this->sendPoll();
                // $this->sendPollToPending();
                break;
            case 'poll_report':
                return $this->sendReport();
                break;
            default:
                Log::info(
                    'Queue type not known: ' . $tracking_data . PHP_EOL
                );
        }
    }

    protected function sendPollToPending()
    {
        // Option group
        $buttons = [];
        $ActionBody = [
            'poll_id' => $this->object_id,
            'answer'  => 'agree'
        ];
        $button = new Button('reply', json_encode($ActionBody), 'Saglasan sam.', 'regular');
        $button->setColumns(6);
        $button->setRows(1);
        $button->setBgColor('#9fd9f1');
        $buttons[] = $button;

        // Option public
        $ActionBody = [
            'poll_id' => $this->object_id,
            'answer'  => 'disagree'
        ];
        $button = new Button('reply', json_encode($ActionBody), 'Nisam saglasan.', 'regular');
        $button->setColumns(6);
        $button->setRows(1);
        $button->setBgColor('#9fd9f1');
        $buttons[] = $button;

        // Option public
        $ActionBody = [
            'poll_id' => $this->object_id,
            'answer'  => 'neutral'
        ];
        $button = new Button('reply', json_encode($ActionBody), 'Suzdržan sam.', 'regular');
        $button->setColumns(6);
        $button->setRows(1);
        $button->setBgColor('#9fd9f1');
        $buttons[] = $button;

        // Create message
        $keyboard = new Keyboard($buttons);
        $message = new Message('text', $keyboard);
        $message->setTrackingData(EventConstants::POLL_ANSWER);

        // Get poll data
        $poll = Poll::getPollById($this->object_id);
        $question = $poll['question'];
        $reprezentative = Participant::getParticipantById($poll['reprezentative_id']);

        // Compile poll question
        $question = 'Odbornik ' . $reprezentative['name'] . ' želi vaše mišljenje na pitanje:' .
        "\n" . "\n" .
        '"' . $question . "'";

        $recipients = Queue::getPrticipantsForType($this->type, $this->object_id);
        foreach ($recipients as $recipient)
        {
            // Get paricipant messenger uid
            $messenger_uid = ParticipantMessenger::getParticipantUidById(1, $recipient['participant_id']);

            // Send poll to recipient
            (new Bot($this->request, $message))
                ->on(new MessageEvent($this->request->timestamp, $this->request->message_token,
                new ViberUser($messenger_uid, $reprezentative['name']), $this->request->message))
                ->replay($question)
                ->send();

            // Apdejtuj queue
            $queue = new Queue;
            $queue->updateQueueStatus($recipient['participant_id'], $this->type, $this->object_id);
        }
    }

    protected function sendPoll()
    {
        // Option group
        $buttons = [];
        $ActionBody = [
            'poll_id' => $this->object_id,
            'answer'  => 'agree'
        ];
        $button = new Button('reply', json_encode($ActionBody), 'Saglasan sam.', 'regular');
        $button->setColumns(6);
        $button->setRows(1);
        $button->setBgColor('#9fd9f1');
        $buttons[] = $button;

        // Option public
        $ActionBody = [
            'poll_id' => $this->object_id,
            'answer'  => 'disagree'
        ];
        $button = new Button('reply', json_encode($ActionBody), 'Nisam saglasan.', 'regular');
        $button->setColumns(6);
        $button->setRows(1);
        $button->setBgColor('#9fd9f1');
        $buttons[] = $button;

        // Option public
        $ActionBody = [
            'poll_id' => $this->object_id,
            'answer'  => 'neutral'
        ];
        $button = new Button('reply', json_encode($ActionBody), 'Suzdržan sam.', 'regular');
        $button->setColumns(6);
        $button->setRows(1);
        $button->setBgColor('#9fd9f1');
        $buttons[] = $button;

        // Create message
        $keyboard = new Keyboard($buttons);
        $message = new Message('text', $keyboard);
        $message->setTrackingData(EventConstants::POLL_ANSWER);

        // Get poll data
        $poll = Poll::getPollById($this->object_id);
        $question = $poll['question'];
        $reprezentative = Participant::getParticipantById($poll['reprezentative_id']);

        // Compile poll question
        $question = 'Odbornik ' . $reprezentative['name'] . ' želi vaše mišljenje na pitanje:' .
        "\n" . "\n" .
        '"' . $question . "'";

        // Get paricipant messenger uid
        $messenger_uid = ParticipantMessenger::getParticipantUidById(1, $this->participant_id);

        // Send poll to recipient
        (new Bot($this->request, $message))
            ->on(new MessageEvent($this->request->timestamp, $this->request->message_token,
            new ViberUser($messenger_uid, $reprezentative['name']), $this->request->message))
            ->replay($question)
            ->send();

        // Apdejtuj queue
        $queue = new Queue;
        $queue->updateQueueStatus($this->participant_id, $this->type, $this->object_id);
    }

    protected function sendReport()
    {
        // Here we want to send all ready poll reports
        //$queues = Queue::getParticipantAllPendingType($this->participant_id, $this->type);
        //foreach ($queues as $queue)
        //{
        //}


        // Check if enough answered to send results to participants
        $sent = ParticipantPoll::getSentCount($this->object_id);
        $answered = ParticipantPoll::getAnsweredCount($this->object_id);

        $sent = !empty($sent) ? $sent : 1;
        if (round($answered / $sent * 100, 0) < EventConstants::POLL_RESULT_MARGIN)
        {
            // Poll report is not ready
            return;
        }

        // Compile poll report
        // Get poll data
        $poll = Poll::getPollById($this->object_id);
        $question = $poll['question'];
        $reprezentative = Participant::getParticipantById($poll['reprezentative_id']);

        $report = 'Trenutni rezultat ankete koju je inicirao odbornik ' . $reprezentative['name'] . ' na pitanje:' .
            "\n" .
            '"' . $question . '"' .
            "\n" .
            'je sledeći:' .
            "\n" . "\n" .
            'ukupno učesnika: ' . $sent .
            "\n" .
            'glasalo: ' . $answered . ' (' . round($answered / $sent * 100, 2) . '%)' .
            "\n" . "\n" .
            'za: ' . $poll['agree'] . ' (' . round($poll['agree'] / $answered * 100, 2) . '%)' .
            "\n" .
            'protiv: ' . $poll['disagree'] . ' (' . round($poll['disagree'] / $answered * 100, 2) . '%)' .
            "\n" .
            'suzdržano: ' . $poll['neutral'] . ' (' . round($poll['neutral'] / $answered * 100, 2) . '%)';
            //"\n" . "\n" .
            //'Jos neki info ovde.';

        // Get paricipant messenger uid
        $messenger_uid = ParticipantMessenger::getParticipantUidById(1, $this->participant_id);

        // Send poll to recipient

        $message = new Message('text', NULL);

        (new Bot($this->request, $message))
            ->on(new MessageEvent($this->request->timestamp, $this->request->message_token,
            new ViberUser($messenger_uid, $reprezentative['name']), $this->request->message))
            ->replay($report)
            ->send();

        // Remove queue
        Queue::deleteQueue($this->participant_id, $this->type, $this->object_id);

        $this->DispatchNext($this->request, $this->participant_id);
    }
}
