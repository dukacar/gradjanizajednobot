<?php

namespace App\Models\DbTables;

use Illuminate\Database\Eloquent\Model;

class Queue extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'queue';

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';

    /**
     * Create new poll
     *
     * @param  string  $reprezentative_id  Reprezentative id
     * @param  string  $group_name         Poll question
     * @return int Poll id
     */
    public function createQueue(int $participant_id, string $type = 'poll', int $object_id, int $status = 0)
    {
        $this->participant_id = $participant_id;
        $this->type = $type;
        $this->object_id = $object_id;
        $this->status = $status;
        $this->save();
        return $this->id;
    }

    /**
     * Update poll type by id
     *
     * @param  int     $poll_id  Poll id
     * @param  string  $type     Poll type
     * @return void
     */
    public function updateQueueStatus(int $participant_id, string $type = 'poll', int $object_id)
    {
        return $this
            ->where('participant_id', '=', $participant_id)
            ->where('type', '=', $type)
            ->where('object_id', '=', $object_id)
            ->update(['status' => 1]);
    }

    /**
     * Check if participant is pending in queue
     *
     * @param  Integer  $participant_id  Participant id
     * @return Bool
     */
    public static function isPrticipantPending(int $participant_id)
    {
        return (bool) self::where('participant_id', '=', $participant_id)
            ->count();
    }

    /**
     * Delte participant queued event
     *
     * @param  Integer  $participant_id  Participant id
     * @return Bool
     */
    public static function deleteQueue(int $participant_id, string $type = 'poll', int $object_id)
    {
        return (bool) self::where('participant_id', '=', $participant_id)
            ->where('type', '=', $type)
            ->where('object_id', '=', $object_id)
            ->delete();
    }

    /**
     * Check if participant is pending in queue
     *
     * @param  Integer  $participant_id  Participant id
     * @return Bool
     */
    public static function getPrticipantsForType(string $type, int $object_id)
    {
        return self::leftJoin('participant_messenger','queue.participant_id','=','participant_messenger.participant_id')
            ->select('queue.participant_id', 'participant_messenger.messenger_uid')
            ->whereRaw('queue.object_id = :object_id', ['object_id' => $object_id])
            ->whereRaw('participant_messenger.subscribed > 0')
            ->whereRaw('queue.status = 0')
            ->get();
    }

    /**
     * Check if participant is pending in queue
     *
     * @param  Integer  $participant_id  Participant id
     * @return Bool
     */
    public static function isPrticipantPendingOtherType(int $participant_id, string $type, int $object_id)
    {
        return (bool) self::where('participant_id', '=', $participant_id)
            ->where('type', '!=', $type)
            ->where('object_id', '!=', $object_id)
            ->where('status', '=', 1)
            ->count();
    }

    /**
     * Check if participant is pending in queue
     *
     * @param  Integer  $participant_id  Participant id
     * @return Bool
     */
    public static function getPrticipantPending(int $participant_id)
    {
        return self::where('participant_id', '=', $participant_id)
            ->where('status', '=', 0)
            ->orderBy('created_at', 'ASC')
            ->limit(1)
            ->first();
    }

    /**
     * Check if participant is pending in queue
     *
     * @param  Integer  $participant_id  Participant id
     * @return Bool
     */
    public static function getParticipantAllPendingType(int $participant_id, string $type)
    {
        return self::where('participant_id', '=', $participant_id)
            ->where('type', '=', $type)
            ->where('status', '=', 0)
            ->orderBy('created_at', 'ASC')
            ->get();
    }
}
