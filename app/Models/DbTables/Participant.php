<?php

namespace App\Models\DbTables;

use Illuminate\Database\Eloquent\Model;

use App\Models\DbTables\ParticipantPoll;
use App\Models\DbTables\ParticipantAnswer;

use DB;

class Participant extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'participant';

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';

    public function participant_messengers()
    {
        return $this->hasMany('ParticipantMessenger');
    }

    /**
     * Get participant by participant id
     *
     * @param  Integer  $participant_id  Participant id
     * @return Participant object
     */
    public static function getParticipantById(int $participant_id)
    {
        return self::where('id', $participant_id)
            ->first();
    }

    /**
     * Get participant type by participant id
     *
     * @param  Integer  $participant_id  Participant id
     * @return String
     */
    public static function getParticipantTypeById(int $participant_id)
    {
        return self::where('id', $participant_id)
            ->value('type');
    }

    /**
     * Get participant type by participant id
     *
     * @param  Integer  $participant_id  Participant id
     * @return String
     */
    public static function updateOnboardingStep(int $participant_id, int $step)
    {
        return self::where('id', $participant_id)
            ->update(['onboarding_step' => $step]);
    }

    /**
     * Get all subscribed participants
     *
     * @return Participants object
     */
    public function getAllSubscribedParticipants()
    {
        return $this
            ->leftJoin('participant_messenger','participant.id','=','participant_messenger.participant_id')
            ->select('participant.id', 'participant_messenger.messenger_uid')
            ->whereRaw('participant_messenger.subscribed > 0 AND participant_messenger.messenger_id = 1')
            ->get();
    }

    /**
     * Get all subscribet participants by organization id
     *
     * @param  Integer  $organization_id  Organization id
     * @return Participant object
     */
    public function getAllSubscribedParticipantsByOrganizationId(int $organization_id)
    {
        return $this
            ->leftJoin('participant_messenger','participant.id','=','participant_messenger.participant_id')
            ->select('participant.id', 'participant_messenger.messenger_uid')
            ->whereRaw('participant_messenger.subscribed > 0 and participant_messenger.messenger_id = 1 and participant.organization_id = :organization_id', ['organization_id' => $organization_id])
            ->get();
    }

    /**
     * Check if participant has recent pending poll
     *
     * @param  Integer  $participant_id     Participant id
     * @param  String   $pending_pool_time  Pending pool offset time
     * @return Bool
     */
    public function participantHasPendingPoll(int $participant_id, string $pending_pool_time = '-1 days')
    {
        return ParticipantPoll::participantHasPendingPoll($participant_id, $pending_pool_time);
    }

    /**
     * Check if participant has recent pending answer
     *
     * @param  Integer  $participant_id     Participant id
     * @param  String   $pending_pool_time  Pending pool offset time
     * @return Bool
     */
    public function participantHasPendingAnswer(int $participant_id, string $pending_pool_time = '-1 days')
    {
        return ParticipantAnswer::participantHasPendingAnswer($participant_id, $pending_pool_time);
    }

    /**
     * Get participant type by participant id
     *
     * @param  Integer  $participant_id  Participant id
     * @return String
     */
    public static function getLastDemoStep(int $participant_id)
    {
        return self::where('id', $participant_id)
            ->value('onboarding_step');
    }
}
