<?php

namespace App\Models\DbTables;

use Illuminate\Database\Eloquent\Model;

class Messenger extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'messenger';
}
