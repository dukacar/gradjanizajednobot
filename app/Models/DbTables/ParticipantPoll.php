<?php

namespace App\Models\DbTables;

use Illuminate\Database\Eloquent\Model;

class ParticipantPoll extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'participant_poll';

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';

    /**
     * Check if participant has recent pending poll
     *
     * @param  Integer  $participant_id  Participant id
     * @return Bool
     */
    public static function participantHasPendingPoll(int $participant_id, string $pending_pool_time = '-1 days')
    {
        return (bool) self::where('participant_id', '=', $participant_id)
            ->where('status', '=', 'sent')
            ->where('sent_time', '>', strtotime($pending_pool_time))
            ->count();
    }

    /**
     * Create new poll
     *
     * @param  string  $reprezentative_id  Reprezentative id
     * @param  string  $group_name         Poll question
     * @return int Poll id
     */
    public function createParticipantPoll(int $participant_id, int $poll_id, string $status = 'sent')
    {
        $this->participant_id = $participant_id;
        $this->poll_id = $poll_id;
        $this->status = $status;
        $this->sent_time = time();
        $this->save();
        return $this->id;
    }

    /**
     * Get organization by organization id
     *
     * @param  Integer  $organization_id  Organization id
     * @return Organization object
     */
    public static function getParicipantPollById(int $poll_id)
    {
        return self::where('id', $poll_id)
            ->first();
    }

    /**
     * Update poll type by id
     *
     * @param  int     $poll_id  Poll id
     * @param  string  $type     Poll type
     * @return void
     */
    public function updatePollAnswer(int $poll_id, int $participant_id, string $answer = 'neutral')
    {
        return $this->where('poll_id', '=', $poll_id)
            ->where('participant_id', '=', $participant_id)
            ->update(['answer' => $answer, 'status' => 'answered', 'answered_time' => time()]);
    }

    /**
     * Update poll type by id
     *
     * @param  int     $poll_id  Poll id
     * @param  string  $type     Poll type
     * @return void
     */
    public function updatePollStatus(int $poll_id, int $participant_id, string $status)
    {
        return $this->where('poll_id', '=', $poll_id)
            ->where('participant_id', '=', $participant_id)
            ->update(['status' => $status]);
    }

    /**
     * Check if participant has recent pending poll
     *
     * @param  Integer  $participant_id  Participant id
     * @return Bool
     */
    public static function getSentCount(int $poll_id)
    {
        return self::where('poll_id', '=', $poll_id)
            ->count();
    }

    /**
     * Check if participant has recent pending poll
     *
     * @param  Integer  $participant_id  Participant id
     * @return Bool
     */
    public static function getAnsweredCount(int $poll_id)
    {
        return self::where('poll_id', '=', $poll_id)
            ->where('status', '=', 'answered')
            ->count();
    }

    /**
     * Check if participant has recent pending poll
     *
     * @param  Integer  $participant_id  Participant id
     * @return Bool
     */
    public function getSentParticipants(int $poll_id)
    {
        return $this
            ->leftJoin('participant_messenger','participant_poll.participant_id','=','participant_messenger.participant_id')
            ->select('participant_poll.participant_id', 'participant_messenger.messenger_uid')
            ->whereRaw('participant_poll.poll_id = :poll_id', ['poll_id' => $poll_id])
            ->get();
    }
}
