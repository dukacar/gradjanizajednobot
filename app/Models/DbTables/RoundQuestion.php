<?php

namespace App\Models\DbTables;

use Illuminate\Database\Eloquent\Model;

class RoundQuestion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'round_question';

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';

    /**
     * Delete all questions for a round
     *
     * @param  int  $round_id  Round id
     * @return void
     */
    public static function deleteRoundQuestionsByRoundId(int $round_id)
    {
        // Get all round questions
        $round_question_ids = self::getRoundQuestionIds($round_id);
        foreach ($round_question_ids as $round_question_id)
        {
            // Delete participant answers
            ParticipantAnswer::deleteAnswerByQuestionId($round_question_id);
        }

        self::where('round_id', $round_id)->delete();
    }

    /**
     * Get round questions ids by round id
     *
     * @param  int  $round_id  Round id
     * @return array
     */
    protected static function getRoundQuestionIds(int $round_id)
    {
        return self::where('round_id', $round_id)
            ->pluck('id')
            ->toArray();
    }
}
