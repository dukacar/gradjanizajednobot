<?php

namespace App\Models\DbTables;

use Illuminate\Database\Eloquent\Model;

use DB;

class DemoAnswer extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'demo_answer';

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';

    /**
     * Create new poll
     *
     * @param  string  $reprezentative_id  Reprezentative id
     * @param  string  $group_name         Poll question
     * @return int Poll id
     */
    public function createDemoAnswer(int $participant_id, string $answer)
    {
        $this->participant_id = $participant_id;
        $this->answer = $answer;
        $this->save();
        return $this->id;
    }
}
