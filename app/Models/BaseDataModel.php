<?php

namespace App\Models;

use Illuminate\Http\Request;

class BaseDataModel
{
    /**
     * Request
     *
     * @var \Illuminate\Http\Request
     */
    protected static $request;

    /**
     * Messenger id
     *
     * @var Int
     */
    protected static $messenger_id;

    /**
     * Messenger uid
     *
     * @var String
     */
    protected static $messenger_uid;

    /**
     * Messenger name
     *
     * @var String
     */
    protected static $messenger_name = '';

    /**
     * Participant
     *
     * @var Participant Object
     */
    protected static $participant;

    /**
     * Participant id
     *
     * @var Int
     */
    protected static $participant_id;

    /**
     * Participant Messenger id
     *
     * @var Int
     */
    protected static $participant_messenger_id;

    /**
     * Participant type
     *
     * @var String
     */
    protected static $participant_type = '';

    /**
     * Organization
     *
     * @var Organization Object
     */
    protected static $organization;

    /**
     * Organization id
     *
     * @var Int
     */
    protected static $organization_id = 0;

    /**
     * Poll
     *
     * @var Poll Object
     */
    protected static $poll;

    /**
     * Poll id
     *
     * @var Int
     */
    protected static $poll_id;

    /**
     * Poll type
     *
     * @var String
     */
    protected static $poll_type = '';

    /**
     * Poll question
     *
     * @var String
     */
    protected static $poll_question = '';

    /**
     * Poll answer
     *
     * @var String
     */
    protected static $poll_answer = '';

    /**
     * Instantiate conversation started
     *
     * @param  Illuminate\Http\Request  $request         Request object
     * @param  String                   $messenger_name  Messenger name
     * @return string
     */
    public function __construct()
    {
        // TODO: Verify request depending on messenger
        //$this->setRequest($request);
        //$this->setMessangerName($messenger_name);
        //$this->messenger_id = $this->getMessengerIdByName();
    }

    public function __get($property)
    {
        if (property_exists($this, $property))
        {
            return self::$$property;
        }
    }

    public function __set($property, $value)
    {
        if (property_exists($this, $property))
        {
            self::$$property = $value;
        }
    }

    /**
     * Verify request data
     *
     * @param  Illuminate\Http\Request  $request  Request object
     * @var string
     */
    public function verifyRequestData()
    {
        return true;
    }
}
