<?php

namespace Paragraf\ViberBot\Messages;

use Paragraf\ViberBot\Http\Http;

use Log;

class WelcomeMessage
{
    protected $user_id;

    protected $type = 'text';

    public function body()
    {
        // picture
        $body = [
            'sender' => [
                'name' => config('viberbot.name'),
                'avatar' => config('viberbot.photo')
            ],
            'tracking_data' => 'welcome',
            'type' => 'picture',
            // 'text' => 'Dobrodošao na KvizBot!',
            'media' => config('viberbot.photo'),
            'thumbnail' => config('viberbot.thumb')
        ];

        // text
        $body = [
            'sender' => [
                'name' => config('viberbot.name'),
                'avatar' => config('viberbot.photo')
            ],
            'tracking_data' => 'welcome',
            'min_api_version' => 1,
            'type' => 'text',
            // 'text' => 'Dobrodošao na KvizBot!',
        ];

        // text keyboard
        $body = [
            'sender' => [
                'name' => config('viberbot.name'),
                'avatar' => config('viberbot.photo')
            ],
            'tracking_data' => 'welcome',
            'type' => 'text',
            // 'text' => 'Dobrodošao na KvizBot!',
            'keyboard' => [
                'Type' => 'keyboard',
                'DefaultHeight' => true
            ],
        ];

        $body['keyboard']['Buttons'] = [(object) [
            'Columns' => 6,
            'Rows' => 1,
            'ActionType' => 'reply',
            'ActionBody' => 'reply to me',
            'Text' => 'Želim da probam!',
            'TextSize' => 'regular',
            'BgColor' => '#9fd9f1'
        ]];

        return $body;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }
}
