<?php

namespace Paragraf\ViberBot\Messages;

use Paragraf\ViberBot\Http\Http;
use Paragraf\ViberBot\Intefaces\MessageInterface;
use Paragraf\ViberBot\Intefaces\MessageSendInterface;

use Log;

class WelcomeMessage implements MessageInterface, MessageSendInterface
{
    protected $user_id;

    protected $type;

    protected $tracking_data = 'tracking data';

    public function body()
    {
        $body = [
            'sender' => [
                'name' => config('viberbot.name'),
                'avatar' => config('viberbot.photo')
            ],
            'tracking_data' => 'tracking data 1',
            'type' => 'picture',
            //'text' => 'Dobrodošao na KvizBot!',
            'media' => config('viberbot.photo'),
            'thumbnail' => config('viberbot.thumb')
        ];

        Log::info(
            'WelcomeMessage BODY: ' . PHP_EOL .
            'data: ' . PHP_EOL .print_r($body, true) . PHP_EOL
        );

        return $body;
    }

    public function send()
    {
Log::info('Sending WelcomeMessage');
        Http::call('POST', '', $this->body());
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function getTrackingData()
    {
        return $this->tracking_data;
    }

    public function setTrackingData($tracking_data)
    {
        $this->tracking_data = $tracking_data;

        return $this;
    }
}
