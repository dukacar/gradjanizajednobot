<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Gradjanski BOT :: Zašto?</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 64px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .center {
              margin: auto;
              width: 50%;
              padding: 10px;
              text-align: left;
              font-size: 13px;
              width: 600px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <!--<div class="title m-b-md">
                    <img src="gradjanskibot_thumb.jpg" alt="Kviz Bot">
                </div>-->

                <div class="center">
                    <p>Obaveza odbornika je da zastupa tvoje interese.<br>
                    Svaka njegova radnja treba da bude otvorena za javnost.<br>
                    Ako tvoji interesi nisu zastupljeni imaš pravo da ga pozoveš na odgovornost!<br>
                    <strong>Da li znaš ijednog gradskog odbornika?</strong><br><br>

                    Rad opštinske uprave je stvar tvog interesa i dužna je da te uključi u odlučivanje.<br>
                    <strong>Da li te je neko ikad pitao za mišljenje?</strong><br>

                    Dužnost odbornika je da te informiše o svim planovima i odlukama koje utiču na tvoje interese.<br>
                    <strong>Da li si primetio da iko uopšte ima nameru da te informiše?</strong><br><br><br>



                    Naša vizija je grad u kome svi građani informisani i uključeni u donošenje odluka.<br><br>

                    Imamo ideju kako da to postane nova realnost, kako da natermo odbornike da, za promenu, malo rade u interesu građana.<br><br>

                    Pravimo sistem u kome nećeš ti morati da juriš informacije nego će informacije i ankete dolaziti do tebe, na tvoj telefon. Na Viber, Fejsbuk mesindžer, itd.<br><br>

                    <strong>Zašto mislimo da će vam vaši odbornici slati informacije i pitati za mišljenje putem ove platforme?</strong><br>
                    Nemamo više poverenja u odbornike (političare) baš kao ni ti jer su nas izneverili svaki put! Ne interesuju nas više njihova prazna obećanja.<br>
                    Računamo sa tim da je njihov lični interes da nas, kao opoziciona manjina, informišu i pitaju kako bi zadobili naše poverenje i naše glasove, da povećaju svoju popularnost. Računamo takođe, da će vrlo brzo i ostali odbornici shvatiti da je ovo način za njihovu ličnu promociju i promociju njihovih političkih partija.<br>
                    Ako se to desi, mi građani smo već pobedili jer više nema nazad. Kako će neko da objasni zašto neki odbornici mogu da uključe građane u odlučivanje a ti, moj odbornik, kome sam dao povernje potpisom, za koga sam glasao ne?<br>
                    Platforma je građanska, potpuno otvorena za sve koji žele da je koriste.<br><br>

                    - Sistem neće cenzurisati nikoga a sami građani će, potpuno anonimno, ocenjivati aktivnosti odbornika.<br>
                    - Odbornici će moći da šalju informacije i prave ankete za građane.<br>
                    - Moći ćeš da posatvljaš pitanja izabranim odbornicima i ocenjuješ njihove odgovore. Ako ne odgovaraju, item će ih „nagraditi“ negativnim poenima i o tome obavestiti sve učesnike.<br><br>

                    Ali kao što ni mi političarima više ne verujemo na reč, nemoj ni ti nama!<br><br>

                    <strong>Skeniraj kod i uveri se sam!</strong><br><br></p>
                  </div>

                <!--
                <div class="links">
                    Skeniraj QR kod ispod i uključi se u odlučivanje u svojoj zajednici!<br><br><br>
                </div>

                <div class="title m-b-md">
                    <img src="qr_indjija.png" alt="Gradjanski Bot QR Code">
                </div>
                -->

                <div class="links">
                    <a href="/">Početna</a>
                    <a href="/kako">Kako da skeniram ovaj kod?</a>
                    <!--
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://blog.laravel.com">Blog</a>
                    <a href="https://nova.laravel.com">Nova</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://vapor.laravel.com">Vapor</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                    -->
                </div>
            </div>
        </div>
    </body>
</html>
