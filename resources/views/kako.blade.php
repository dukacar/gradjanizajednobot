<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Gradjanski BOT :: Kako?</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 64px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            #outer-div {
            width: 100%;
            text-align: center;
          }
          #inner-div {
            display: inline-block;
            margin: 0 auto;
            padding: 3px;
          }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    <img src="gradjanskibot_thumb.jpg" alt="Kviz Bot">

                    <div id="outer-div">
                      <div id="inner-div"><img src="web_uputstvo_01.jpg" alt="Uputstvo za skeniranje koda 01"></div>
                      <div id="inner-div"><img src="web_uputstvo_02.jpg" alt="Uputstvo za skeniranje koda 02"></div>
                    </div>
                </div>

                <!--<div class="title m-b-md">
                    Platforma je još u fazi razvoja.<br> Obavestićemo te preko platforme kada strana bude u funkciji.<br> Zasada oprosti!
                </div>-->

                <!--
                <div class="links">
                    Skeniraj QR kod ispod i uključi se u odlučivanje u svojoj zajednici!<br><br><br>
                </div>

                <div class="title m-b-md">
                    <img src="qr_indjija.png" alt="Gradjanski Bot QR Code">
                </div>
                -->

                <div class="links">
                    <a href="/">Početna</a>
                    <a href="/zasto">Čemu ovo?</a>
                    <!--
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://blog.laravel.com">Blog</a>
                    <a href="https://nova.laravel.com">Nova</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://vapor.laravel.com">Vapor</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                    -->
                </div>
            </div>
        </div>
    </body>
</html>
